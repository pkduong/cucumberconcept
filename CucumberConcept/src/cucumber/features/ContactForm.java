package cucumber.features;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ContactForm {
	
	WebDriver driver = null;
	private String urlString = "http://www.thetestroom.com/webapp/";

	@Given("^I am on my Zoo website$")
	public void i_am_on_my_Zoo_website() throws Throwable {
		//Selenium v.2
		driver = new FirefoxDriver();
		driver.navigate().to(urlString);
		
		//Selenium v.3
		//driver = new FirefoxDriver();
		//System.setProperty("webdriver.firefox.marionette", "./geckodriver.exe");
		//driver.get(urlString);
		
	}

	@When("^I click on Contact link$")
	public void i_click_on_Contact_link() throws Throwable {
		driver.findElement(By.id("contact_link")).click();
	}

	@When("^populate the contact form$")
	public void populate_the_contact_form() throws Throwable {
		driver.findElement(By.name("name_field")).sendKeys("Duong Pham");
		driver.findElement(By.name("address_field")).sendKeys("Adress Happy Land");
		driver.findElement(By.name("postcode_field")).sendKeys("12345");
		driver.findElement(By.name("email_field")).sendKeys("pkduong@gmail.com");
		
		driver.findElement(By.id("submit_message")).click();
	}
	
	@When("^populate the contact form using DataTable$")
	public void populate_the_contact_form_using_DataTable(DataTable table) throws Throwable {
		
		List<List<String>> data = table.raw();
		
		driver.findElement(By.name("name_field")).sendKeys(data.get(1).get(1));
		driver.findElement(By.name("address_field")).sendKeys(data.get(2).get(1));
		driver.findElement(By.name("postcode_field")).sendKeys(data.get(3).get(1));
		driver.findElement(By.name("email_field")).sendKeys(data.get(4).get(1));
		
		driver.findElement(By.id("submit_message")).click();
	}

	@Then("^I should be on the contact confirmation page$")
	public void i_should_be_on_the_contact_confirmation_page() throws Throwable {
		Assert.assertTrue("Not on Contact Confirmation page",driver.getTitle().equals("Contact Confirmation"));
	}

}
