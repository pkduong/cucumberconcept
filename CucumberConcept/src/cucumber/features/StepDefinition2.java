package cucumber.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition2 {
	
	@Given("^I navigate to the zoo website$")
	public void i_navigate_to_the_zoo_website() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("Navigate to zoo website");
	}


	@When("^I click on about link$")
	public void i_click_on_about_link() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    System.out.println("executed to click on ABOUT link");
	}

}
