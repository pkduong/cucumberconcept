package cucumber.features;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.testng.Assert;
import junit.framework.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PageTitleCheck {
	
	WebDriver driver = null;
	private String urlString = "http://www.thetestroom.com/webapp/";
	
	@Given("^I am on the zoo website$")
	public void i_am_on_the_zoo_website() throws Throwable {
		//Selenium v.2
		driver = new FirefoxDriver();
		driver.navigate().to(urlString);
	}
	
	@When("^I navigate to \"([^\"]*)\" page$")
	public void i_navigate_to_page(String link) throws Throwable {
		driver.findElement(By.id(link)).click();
	}

	@Then("^I check page title is \"([^\"]*)\"$")
	public void i_check_page_title_is(String title) throws Throwable {
		Assert.assertTrue(driver.getTitle().contains(title));
	}

	@Then("^I close browser$")
	public void i_close_browser() throws Throwable {
		driver.close();
	}
}
