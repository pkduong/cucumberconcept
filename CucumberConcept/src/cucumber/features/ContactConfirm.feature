Feature: To test contact form works when there are no errors

  Scenario: Check form is validated when there are no errors
    Given I am on my Zoo website
    When I click on Contact link
    And populate the contact form
    Then I should be on the contact confirmation page

  Scenario: Demo of DataTable
    Given I am on my Zoo website
    When I click on Contact link
    And populate the contact form using DataTable
      | Fields    | Values            |
      | Name      | Billy Joe         |
      | Address   | HCM City          |
      | Post Code | A1 S22            |
      | Email     | duongpk@gmail.com |
    Then I should be on the contact confirmation page
